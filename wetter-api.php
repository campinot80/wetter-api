<?php

    $url = "https://api.openweathermap.org/data/2.5/weather?q=leipzig&appid=0174081f51277e45a0105e37b79b22eb&lang=de&units=metric";

    $json_wetter = file_get_contents($url);

    $jsondata = json_decode($json_wetter);

    $location = $jsondata->name;
    $temp = $jsondata->main->temp;
    $desc_0 = $jsondata->weather;
    $desc = $desc_0[0]->description;

    echo "Die Temperatur in <b>" . $location . "</b> beträgt gerade: <b>" . $temp . "</b>°C";
    echo "<br>";
    echo "<b>Beschreibung:</b> " . $desc;

?>